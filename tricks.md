### Tricks

1. Debugging/ Inspecting if any POD(s) fail to start.

```debug
1. As a first step describe pod
run "kubectl describe pod/<pod-name>"

2. if from step 1, we are not able to figure out the problem then check logs
run "kubectl logs [-f] pod/<pod-name>"

```

```trick
meaning:

- when we are running k8s on a host, it runs 2 copy of docker
  - one local docker server
  - second within k8s cluster

refer - udemy/docker-and-kubernetes-the-complete-guide/section13-210 to 211
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/multi-docker

Q. how to configure?
A.
[MAC] 
open a terminal > run "eval $(minikube docker-env)" 

refer - udemy/docker-and-kubernetes-the-complete-guide/section13-210 to 211

```

[ONLY VALID FOR MAC/LINUX]
[HOW TO DO SAME FOR WINDOWS ?]
2. Re-configuring CLI to use docker server inside k8s cluster.

```trick
meaning:

- when we are running k8s on a host, it runs 2 copy of docker
  - one local docker server
  - second within k8s cluster

refer - udemy/docker-and-kubernetes-the-complete-guide/section13-210 to 211
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/multi-docker

Q. how to configure?
A.
[MAC] 
open a terminal > run "eval $(minikube docker-env)" 

refer - udemy/docker-and-kubernetes-the-complete-guide/section13-210 to 211

```