### Volumes

Its always associated with persisted db container / pods.

```d
Definition
- every db docker container/pod has isolated file-system which is only
accessible by the container.

- db container use these isolated file-system for storing persisted data

refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/section14/s14-228-volumes
page - postgres-1

- If some how Pod crashes whole persisted data gets lost
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/section14/s14-228-volumes
page - postgres-2
[THIS IS A PROBLEM , SINCE POD GOES DOWN IS NORMAL IN k8s]

```

```volumes
- to resolve above problem  "VOLUMES" are introduced

- these are file-system of host m/c which is used by PODS to persist data,
  and share among sibling PODS

refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/section14/s14-228-volumes
page - postgres-3

NOTE: for db containers on scaling replicas it's not recommened that all pods share
common volume(s). -- WHY ?

```
