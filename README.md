### udemy/cources/docker_and_kubernetes_the_complete_guide

#### Git Repo - https://github.com/StephenGrider/DockerCasts

#### From Section 12: Onwards to k8s

#### Warnings/ Notes

```youtrack
1. During section14, 15 we used github-travis CI pipelines
specific to that we add "./section14-and-15/travis-cli" directory.
[DO NOT REFER THAT IF YOUR ARE NOT USING GITHUB-TRAVIS CI/CD PIPELINE]


This folder is useful only if we are using
  - github-travis CI-CD PIPELINE
  - and using encrypted secrets to connect to GCP

IF ABOVE WARNING MENTIONED IS YOUR USE CASE, then please follow this doc.

```