### k8s deployment types
There are Two types of deployment
1. Declarative Deployments (Recommended)
2. Imperative deployments (use only if Declarative Deployments doesn't ful-fill your use case )

#### 1. Declarative Deployments
```dd
here we give total control to master to deploy 'n' number of container(s) as per master 
suitability.

Note: this is far easier process then  Imperative deployments

```

#### 2. Imperative Deployments
```id
here we take control from master to deploy 'n' number of container(s) as per master suitability.
we tell master to do exactly as per our use case.

Note: this is really complicated process, use only if Declarative Deployments 
doesn't ful-fill your use case.

refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/section12/s12-195-deployments

```

### Declarative Deployments vs Imperative Deployments
```vs
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/section12/s12-195-deployments 
      - page - "imperative vs declarative"
```