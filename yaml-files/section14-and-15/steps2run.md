### Steps to run this section docker container in pod and access it on localhost/"minikube ip"

#### To run app in docker
- use docker-compose up to build app and run in docker
- then use k8s to orchestrate it

```docker
1. "cd poc-project-docker" - here it's same project running as independent docker sontainers
2. "docker-compose up"
3. access http://localhost:3050/
```

#### To run app in k8s-cluster

```obsolete
[OBSOLETE]
kubectl apply -f client-pod.yaml

- this step from section12 is obsolete since we udes k8s deployment object using  
  "client-deployment.yaml" file
```

1. kubectl apply -f client-deployment.yaml
2. kubectl apply -f client-node-port.yaml
3. find minikube ip -
4. access "minikubeIp/nodePort"

##### Note:
if you are runnng docker desktop and config it to start k8s clust at the time of docker start.<br>
Skip Step 3 above and use "http://localhost/nodeport" or "http://127.0.0.1:31516/"