### 1. minicube

- is a k8s command line tool whose sole purpose is to create k8s cluster <br />
               for **DEVELOPMENT**(not **PROD**) env.<br/>

- uses for managing VM/cluster itself

- For **PROD** we used managed solution like **EKS**(Amazon), **GKE**(Google) etc. <br />

### 2. minikube commands

```minikube-commands

 - 2.1 - "minikube start"
 - this create k8s cluster
 - this start minikube k8s services


 - 2.2 - "minikube stop"
 - this stops minikube k8s services

 - 2.3 - "minikube delete"
 - this deletes minikube k8s services

 - 2.4 - "minikube status"
 - to check minkube running status
 
 - 2.5 - "minikube ip"
 - ip of minikube where we test running apply
 - e.g. http://127.0.0.1:8080 
 
``` 
 
### 3. kubectl commands

refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/kubectl-commands

- is a k8s command line tool whose purpose is managing containers in the node <br />
in **DEV** as well as in **PROD**.

```kubectl-commands
 - 3.1 - "kubectl get all [-n <namespane name>]"
 - this returns everything (like pods etc) that we have defined in localhost k8s cluster
 - "-n <namespane name>" is OPTIONAL, if we do not specify anyting it take "deafult" namespace
----------------------------------------------------------------------------------------- 
 [Output]
NAME         READY   STATUS              RESTARTS   AGE
pod/webapp   0/1     ContainerCreating   0          15s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   3d20h
----------------------------------------------------------------------------------------- 

 - 3.2 - "kubectl apply -f "<yaml-file>"
 - this create k8s object after reading the specified yaml file 
 - when we run this command then either its an Create or Update operation
   - Update - if k8s find "name +kind" as per specified k8s config file
   - Create - if k8s doesn't find "name+kind" as per specifid config file

 -3.3 - "kubectl get <po/pods/services/svc/all>" [-o wide] [--show-labels]
 - to get status of pods/srvices/all running k8s objects 
 - svc is an alias of services
 - po is an alias of pods
   -- "get all svc" is same as "get all services" 
   -- "kubectl get po --show-labels"

-3.3 - "kubectl set image <object-type>/<object-name> <container-name>=<new-image-to-use>
- eg "kubectl set image deployment.apps/client-deployment client=stephengrider/multi-client:v5" 
- it use to update all the pod(s) with name "client" to use "stephengrider/multi-client:v5" docker image
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/kubectl-commands
       page - "set image"

- 3.4 - "kubectl delete name(s)
 3.4.1
 - this deletes k8s object after reading specified name(s).
 - e.g.  "kubectl delete pod/client-pod service/client-node-port" 
 
 3.4.2 - kubectl delete <k8s-object-type> --all
   - this delete all <k8s-object-type> from k8s cluster
   - all is the flag
   - eg. "kubectl delete pods --all"
 3.4.3 - kubectl delete -f <directory-of-k8s-object>
   - this delete all <k8s-objects> from a directory
 
[IMP]
 - 3.5 
   
   -- 3.5.1 - "kubectl describe <kind-of-k8s-obj> <name-of-k8s-obj>"
   -- eg "kubectl describe pod webapp"
   -- <kind-of-k8s-obj> - we get this from k8s yaml file
   
   -- 3.5.2 - "kubectl describe <kind-of-k8s-obj>/<name-of-k8s-obj>"
   -- eg "kubectl describe pod/webapp"
   -- <kind-of-k8s-obj> - we get this from k8s yaml file
   
 - 3.6 - "kubectl [-it] exec <name-of-k8s-pod> <linux-command-that-we-want-to-run-inside-pod>"
 - eg. "kubectl exec webapp ls",  -- its almost same as docker for same purpose
       "kubectl exec webapp whoami" etc.
 [IMP]
 - eg to ssh into k8s-pod "kubectl -it exec webapp sh"

 - 3.7 - "kubectl logs <linux-command-that-we-want-to-run-inside-pod>"
 - to get log of conatianer running in pod
  
 - 3.8 kubectl cluster-info
 - it tells us whwre k8s cluster is running.

 - 3.9 "kubectl get namespaces" or "kubectl get ns"
 - it return all the namespaces present in localhost.
 - is a way of partitioning k8s resources into separate areas.
 - Use "kubectl get all -n <namespane name>"
   -- "-n <namespane name>" is OPTIONAL, if we do not specify anyting it take "deafult" namespace
```