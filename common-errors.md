### Common Errors
```ce
1. The Pod "client-pod" is invalid: spec: Forbidden: pod updates may not change fields other than 
`spec.containers[*].image`, 
`spec.initContainers[*].image`, 
`spec.activeDeadlineSeconds` or 
`spec.tolerations` (only additions to existing tolerations)

Cause - 

Resolution 
R1 - one way is to delete this k8s object and re create using "kubectl apply" or
R2 - we need to use k8s "Deployment" object.
    - when we use Deployment object , it also internally deletes the pod then restart it

refer - udemy/docker-and-kubernetes-the-complete-guide/s13-198 to s13-200
refer - kaushiwould/googledrive/draw.io/k8s/udemy/docker-and-kubernetes-the-complete-guide/Restrictions

2. The Deployment "client-deployment" is invalid: spec.selector: 
Invalid value: v1.LabelSelector{MatchLabels:map[string]string{"component":"web"}, 
MatchExpressions:[]v1.LabelSelectorRequirement(nil)}: field is immutable 

cause:

Resolution: delete the k8s deployment object and recreate it using "kubectl apply"
```
